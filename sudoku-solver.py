# -*- coding: utf-8 -*-
import sys
sys.path.append("./python-constraint-1.2")
import constraint as csp

import backtrack
import time
import math


# ------------------------------------------------------------------------------
# sudoku to solve (add "0" where no number is given)
# ------------------------------------------------------------------------------
import riddles

riddle = riddles.beginning
NUMBER_OF_SOLVES = 100

# ------------------------------------------------------------------------------
# create helpful lists of variable names
# ------------------------------------------------------------------------------
rownames = ["a", "b", "c", "d", "e", "f", "g", "h", "i"]
colnames = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

rows = []
for i in rownames:
    row = []
    for j in colnames:
        row.append(i+j)
    rows.append(row)

cols = []
for j in colnames:
    col = []
    for i in rownames:
        col.append(i+j)
    cols.append(col)

boxes = []
for x in range(3):  # over rows of boxes
    for y in range(3):  # over columns of boxes
        box = []
        for i in range(3):  # over variables in rows (in a box)
            for j in range(3):  # over variables in cols (in a box)
                box.append(rownames[x*3 + i] + colnames[y*3 + j])
        boxes.append(box)

# ------------------------------------------------------------------------------
# Execute solve algorithm defined times and print time statistics
# ------------------------------------------------------------------------------
def execute_statistics():
    solver_times = list()
    sudoku = csp.Problem()
    my_solver = backtrack.Backtrack()
    formulate_riddle(riddle, sudoku, my_solver)
    for i in range(NUMBER_OF_SOLVES):
        start_time = time.time()
        my_solution = my_solver.solve()
        end_time = time.time() - start_time
        solver_times.append(end_time)
        if(my_solution is None):
            print("N/A!")
    
    sum_time = 0
    highest_time = -math.inf
    lowest_time = math.inf
    for solver_time in solver_times:
        sum_time += solver_time
        highest_time = max(highest_time, solver_time)
        lowest_time = min(lowest_time, solver_time)
    
    print("Average Time: ",(sum_time / len(solver_times)))
    print("Worst Time: ",highest_time)
    print("Best Time: ",lowest_time)
    
# ------------------------------------------------------------------------------
# execute statistic for hybrid_threshold and save it to file
# ------------------------------------------------------------------------------
def execute_hybrid_statisics():
    data_list = list()
    for test_riddle in riddles.riddles:
        sudoku = csp.Problem()
        my_solver = backtrack.Backtrack()
        formulate_riddle(test_riddle, sudoku, my_solver)
        for j in range(0, 81):
            solver_times = list()
            for i in range(NUMBER_OF_SOLVES):
                start_time = time.time()
                my_solution = my_solver.solve(j)
                end_time = time.time() - start_time
                solver_times.append(end_time)
                if(my_solution is None):
                    print("N/A!")
            
            sum_time = 0
            highest_time = -math.inf
            lowest_time = math.inf
            for solver_time in solver_times:
                sum_time += solver_time
                highest_time = max(highest_time, solver_time)
                lowest_time = min(lowest_time, solver_time)
            data_list.append(str(sum_time / len(solver_times)))
        data_list.append("-----")
        print("1 riddle solved for threshold 0-80")
    with open("threshold_data.json", "w") as fp:
        for data_line in data_list:
            fp.write(data_line)
            fp.write("\n")

# ------------------------------------------------------------------------------
# starts and stops time for custom backtracking and library solution.
# ------------------------------------------------------------------------------
def execute_review():
    sudoku = csp.Problem()
    my_solver = backtrack.Backtrack()
    formulate_riddle(riddle, sudoku, my_solver)
    
    # ------------------------------------------------------------------------------
    # stop time and print my solution
    # ------------------------------------------------------------------------------
    start_time = time.time()
    my_solution = my_solver.solve()
    end_time = time.time()
    print("\n\nmy solution:")
    print("solved in:",end_time - start_time)
    for row in rows:
        print('[', end = '')
        for cell in row:
            print(my_solution[cell], end = ', ')
        print('],')

    # ------------------------------------------------------------------------------
    # stop time and print library solution
    # ------------------------------------------------------------------------------
    library_start_time = time.time();
    library_solution = sudoku.getSolutions()[0]
    library_solution.values();
    library_time = time.time() - library_start_time;
    print("\n\nlibrary time: ",library_time)
    for row in rows:
        print('[', end = '')
        for cell in row:
            print(library_solution[cell], end = ', ')
        print('],')
        
    # ------------------------------------------------------------------------------
    # Are results between custom backtracking and library identical?
    # ------------------------------------------------------------------------------
    isCorrect = True
    for row in rows:
        for cell in row:
            if(library_solution[cell] != my_solution[cell]):
                isCorrect = False
    if isCorrect:
        print("\nresults are identical :-)")
    else:
        print("\nresults are not identical :-(")

# ------------------------------------------------------------------------------
# formulate sudoku as CSP
# ------------------------------------------------------------------------------
def formulate_riddle(new_riddle, sudoku, my_solver):
    for row in range(len(new_riddle)):
        for col in range(len(new_riddle[row])):
            if(new_riddle[row][col] == 0):
                sudoku.addVariable(rows[row][col], [1, 2, 3, 4, 5, 6, 7, 8, 9])
                my_solver.addVariable(rows[row][col], [1, 2, 3, 4, 5, 6, 7, 8, 9])
            else:
                sudoku.addVariable(rows[row][col], [new_riddle[row][col]])
                my_solver.addVariable(rows[row][col], [new_riddle[row][col]])      
    for row in rows:
        sudoku.addConstraint(csp.AllDifferentConstraint(), row)
        my_solver.addConstraint(backtrack.UniqueConstraint(), row)
    for col in cols:
        sudoku.addConstraint(csp.AllDifferentConstraint(), col)
        my_solver.addConstraint(backtrack.UniqueConstraint(), col)
    for box in boxes:
        sudoku.addConstraint(csp.AllDifferentConstraint(), box)
        my_solver.addConstraint(backtrack.UniqueConstraint(), box)

# ------------------------------------------------------------------------------
# solve CSP
# ------------------------------------------------------------------------------
#execute_statistics()
#execute_hybrid_statisics()
execute_review()
