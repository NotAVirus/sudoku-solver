# sudoku-solver
Ein Sudoku Löser, der Sudokus auf 2 Arten lösen kann:
- Selbst programmierter Backtracking Algorithmus
- Sudoku als Constraint Satisfactory Problem definieren und mittel CSP Library lösen lassen
