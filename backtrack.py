import copy
import math

class Backtrack:
    def __init__(self):
        self._variables = {}
        self._legalValues = {}
        self._constraints = []
        self.current_best = 0
        self._hybrid_threshold = 34
    
    def addVariable(self, variable, domain):
        self._variables[variable] = domain
        
    def addConstraint(self, constraint, variables):
        self._constraints.append((constraint, variables))
    
    def solve(self, hybrid_threshold = 34):
        self._legalValues = copy.deepcopy(self._variables)
        self._hybrid_threshold = hybrid_threshold
        return self.backtrack_search({})

    def backtrack_search(self, assignment):
        if(len(assignment) == 81):
            return assignment
        
        # select new variable
        new_variable = self.select_unassigned_variable(assignment)
        
        # try different values
        for domain_value in self._legalValues[new_variable]:
            # assign variable
            assignment[new_variable] = domain_value

            if(len(assignment) < self._hybrid_threshold):
                inferences = self.forward_check(assignment, new_variable)
            else:
                inferences = self.arc_consistency(assignment, new_variable)
            if(inferences is not None):
                result = self.backtrack_search(assignment)
                if(result is not None):
                    return result
                self.restore_legal_values(inferences)
            assignment.pop(new_variable)
        
        return None
    
    def select_unassigned_variable(self, assignment):
        # Minimum Remaiming Values
        # Degree Heuristic not usable for Sudoku?, because all Variables have 3 Constraints (row, col, box)
        left_assignments = { k : self._legalValues[k] for k in set(self._legalValues) - set(assignment) }
        variable = None
        lowestValue = math.inf
        for key, value in left_assignments.items():
            length = len(value)
            if(length == 1):
                return key
            if(length < lowestValue):
                variable = key
                lowestValue = length
        return variable
    
    def check_consistency(self, assignment, new_variable):
        for constraint in self._constraints:
            if(new_variable in constraint[1] and not constraint[0].check_constraint(assignment, constraint[1])):
                return False
                
        return True
    
    def forward_check(self, assignment, new_variable):
        inferences = []
        new_value = assignment[new_variable]
        for constraint in self._constraints:
            if(new_variable in constraint[1]):
                for variable in constraint[1]:
                    if(variable != new_variable and new_value in self._legalValues[variable]):
                        self._legalValues[variable].remove(new_value)
                        inferences.append((variable, new_value))
        return inferences
    
    def arc_consistency(self, assignment, new_variable):
        inferences = []
        # check all arcs
        for neighbour in self.get_neighbours(new_variable, assignment):
            variable_1 = new_variable
            variable_2 = neighbour
            # revise arc
            self.revise_arc_consistency(assignment, variable_1, variable_2, inferences)
            # when variable_2 has no domains and is not yet assigned, then value is illegal for variable_1
            if(len(self._legalValues[variable_2]) == 0 and variable_2 not in assignment):
                self.restore_legal_values(inferences)
                return None
        return inferences
    
    def revise_arc_consistency(self, assignment, variable_1, variable_2, inferences):
        value = assignment[variable_1]
        if(value in self._legalValues[variable_2]):
            self._legalValues[variable_2].remove(value)
            inferences.append((variable_2, value))
        
    def get_neighbours(self, variable, assignment):
        result = []
        for constraint in self._constraints:
            if(variable in constraint[1]):
                for constraint_variable in constraint[1]:
                    if(variable != constraint_variable and assignment[variable] in self._legalValues[constraint_variable]):
                        result.append(constraint_variable)
        return result

    def restore_legal_values(self, inferences):
        for inference in inferences:
            variable = inference[0]
            value = inference[1]
            self._legalValues[variable].append(value);

class UniqueConstraint:
    def check_constraint(self, assignment, variables):
        unique_values = list()
        for variable in variables:
            if(variable in assignment):
                if(assignment[variable] not in unique_values):
                    unique_values.append(assignment[variable])
                else:
                    return False
        return True